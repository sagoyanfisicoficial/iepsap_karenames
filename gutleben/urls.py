from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',

    #grappelli
    url(r'^grappelli/', include('grappelli.urls')),  # grappelli URLS
	url(r'^inicio/$','principal.views.inicio',name='inicio'),
    #admision
    url(r'^admision/$','principal.views.admision_index',name='admision'),
    url(r'^admision/inicial/$','principal.views.educacion_inicial',name='inicial'),
    url(r'^admision/primaria/$','principal.views.educacion_primaria',name='primaria'),
    url(r'^admision/secundaria/$','principal.views.educacion_secundaria',name='secundaria'),
    #nosotros
    url(r'^nosotros/$','principal.views.nosotros_index',name='nosotros'),
    url(r'^nosotros/vision-mision/$','principal.views.nosotros_mision',name='vision'),
    url(r'^nosotros/mensaje-director/$', 'principal.views.nosotros_director',name='mensaje'),
    url(r'^nosotros/resena-historica/$', 'principal.views.nosotros_historia',name='resena'),
    url(r'^nosotros/plan-estudio/$', 'principal.views.nosotros_plan',name='plan'),
    #talleres
    url(r'^talleres/$', 'principal.views.talleres',name='talleres'),
    url(r'^talleres/computacion/$', 'principal.views.talleres_computacion',name='computacion'),
    url(r'^talleres/educacion-fisica/$', 'principal.views.talleres_educacion_fisica',name='educacion'),
    url(r'^talleres/ingles/$', 'principal.views.talleres_ingles',name='ingles'),
    url(r'^talleres/ballet/$', 'principal.views.talleres_ballet',name='ballet'),
    url(r'^talleres/karate/$', 'principal.views.talleres_karate',name='karate'),
    #noticias
    url(r'^noticias/$', 'principal.views.noticias',name='noticias'),
    #galeria
    url(r'^galeria/$', 'principal.views.galeria',name='galeria'),
    #aula virtual
    url(r'^aulta-virtual/$', 'principal.views.aula_virtual',name='aula-virtual'),





    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$','django.views.static.serve',
		{'document_root':settings.MEDIA_ROOT,}
	),
    url(r'^contacto/$','principal.views.contacto',name='contacto'),
    url(r'^$','principal.views.inicio_web'),
    url(r'^nosotros/$','principal.views.nosotros'),
    #url(r'^patrocinadores/$','principal.views.patrocinador'),
    #url(r'^eventos/$','principal.views.eventos'),
    #url(r'^foro/$','principal.views.foro'),
    #url(r'^usuario/nuevo$','principal.views.nuevo_usuario'),
    #url(r'^ingresar/$','principal.views.ingresar'),
    url(r'^privado/$','principal.views.privado'),
    #url(r'^cerrar/$', 'principal.views.cerrar'),
)
