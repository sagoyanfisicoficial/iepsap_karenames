from principal.models import Noticia,Galeria,NoticiasAdmin
from principal.forms import  ContactoForm
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404,render
from django.template import RequestContext
from django.core.mail import EmailMessage
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from cloudinary import api
import json


def admision_index(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('admision_index.html',context_instance=RequestContext(request))

#modulo de inicial
def educacion_inicial(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('educacion_inicial.html',context_instance=RequestContext(request))
def educacion_primaria(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('educacion_primaria.html',context_instance=RequestContext(request))
def educacion_secundaria(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('educacion_secundaria.html',context_instance=RequestContext(request))


#@nosotros
def nosotros_index(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('nosotros_index.html',context_instance=RequestContext(request))

def nosotros_mision(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('nosotros_visionmision.html',context_instance=RequestContext(request))

def nosotros_director(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('nosotros_director.html',context_instance=RequestContext(request))
def nosotros_historia(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('nosotros_historia.html',context_instance=RequestContext(request))

def nosotros_plan(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('nosotros_plan.html',context_instance=RequestContext(request))


#inicio
def inicio_web(request):
    noticias = Noticia.objects.all()
    return render_to_response('base.html',{'noticias':noticias},context_instance=RequestContext(request))

#@AULA VIRTUAL

def aula_virtual(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('aula_virtual.html',context_instance=RequestContext(request))

#@ TALLERES

def talleres(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('talleres_index.html',context_instance=RequestContext(request))

def talleres_computacion(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('talleres_computacion.html',context_instance=RequestContext(request))

def talleres_educacion_fisica(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('talleres_fisica.html',context_instance=RequestContext(request))

def talleres_ingles(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('talleres_ingles.html',context_instance=RequestContext(request))

def talleres_karate(request):
    #proyectos = Proyectogutleben.objects.all()
    return render_to_response('talleres_karate.html',context_instance=RequestContext(request))

def talleres_ballet(request):
    #noticias = NoticiasAdmin.objects.all()
    return render_to_response('talleres_ballet.html',context_instance=RequestContext(request))

#@NOTICIAS
def noticias(request):
    noticiasadmin = NoticiasAdmin.objects.all()
    return render_to_response('noticias_index.html',{'noticiasadmin':noticiasadmin},context_instance=RequestContext(request))

#@ GALERIA
def galeria(request):
    galeria = Galeria.objects.all()
    return render_to_response('galeria_index.html',{'galeria':galeria},context_instance=RequestContext(request))




@login_required(login_url='/ingresar')
def inicio(request):
    proyectos = Noticia.objects.all()
    return render_to_response('principal.html',{'proyectos':proyectos}, context_instance=RequestContext(request))

@login_required(login_url='/ingresar')
def nosotros(request):
    nosotros = Nosotros.objects.all()
    team= Team.objects.all()
    return render_to_response('nosotros.html',{'nosotros':nosotros,'team':team}, context_instance=RequestContext(request))


@login_required(login_url='/ingresar')
def eventos(request):
    return render_to_response('eventos.html', context_instance=RequestContext(request))

@login_required(login_url='/ingresar')
def foro(request):
    return render_to_response('foro.html', context_instance=RequestContext(request))



#@login_required(login_url='/ingresar')
def contacto(request):
    if request.method=='POST':
        formulario = ContactoForm(request.POST)
        if formulario.is_valid():
            titulo = 'Envio de mensaje de la plataforma Gutleben'
            contenido = formulario.cleaned_data['mensaje'] + "\n"
            contenido += 'Enviar e-mail: ' + formulario.cleaned_data['correo']
            correo = EmailMessage(titulo, contenido, to=['nicolle.11.2006@gmail.com'])
            correo.send()
            return HttpResponseRedirect('/')
    else:
        formulario = ContactoForm()
    return render_to_response('contactoform.html',{'formulario':formulario}, context_instance=RequestContext(request))



def nuevo_usuario(request):
    if request.method=='POST':
        formulario = UserCreationForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return HttpResponseRedirect('/')
    else:
        formulario = UserCreationForm()
    return render_to_response('nuevousuario.html',{'formulario':formulario}, context_instance=RequestContext(request))

def ingresar(request):
    if not request.user.is_anonymous():
        return HttpResponseRedirect('/')
    if request.method == 'POST':
        formulario = AuthenticationForm(request.POST)
        if formulario.is_valid:
            usuario = request.POST['username']
            clave = request.POST['password']
            acceso = authenticate(username=usuario, password=clave)
            if acceso is not None:
                if acceso.is_active:
                    login(request, acceso)
                    return HttpResponseRedirect('/')
                else:
                    return render_to_response('noactivo.html', context_instance=RequestContext(request))
            else:
                return render_to_response('nousuario.html', context_instance=RequestContext(request))
    else:
        formulario = AuthenticationForm()
    return render_to_response('ingresar.html',{'formulario':formulario}, context_instance=RequestContext(request))

@login_required(login_url='/ingresar')
def privado(request):
    usuario = request.user
    return render_to_response('privado.html', {'usuario':usuario}, context_instance=RequestContext(request))

@login_required(login_url='/ingresar')
def cerrar(request):
    logout(request)
    return HttpResponseRedirect('/')

def filter_nones(d):
    return dict((k,v) for k,v in d.iteritems() if v is not None)


