import cloudinary


def resize(request):
    return dict(
        PROYECTOSs = dict(
            format="png",
            transformation=[
                dict(height=191, width=255),
            ]
        ),
        NOTICIAS = dict(
            format="png",
            transformation=[
                dict(height=400, width=700),
            ]
        ),
        ICON_EFFECTS3 = dict(
            format="png",
            type="facebook",
            transformation=[
                dict(height=95, width=95, crop="thumb", gravity="face", effect="sepia", radius=20),
                dict(angle=10),
            ]
        ),
        PROYECTOS = {
            "class": "img-responsive", "format": "jpg", "crop": "fill", "height": 191, "width": 255,
        },
        NOSOTROS = {
            "class": "img-responsive", "format": "jpg", "crop": "fill", "height": 345, "width": 690,
        },
        TEAM = {
            "class": "img-responsive", "format": "jpg", "crop": "fill", "height": 250, "width": 300,
        },
        CLOUDINARY_CLOUD_NAME = cloudinary.config().cloud_name
    )
