#encoding:utf-8
from django.db import models
from django.contrib.auth.models import User
from cloudinary.models import CloudinaryField


class Noticia(models.Model):

	titulo=models.CharField("Titulo",max_length=200)
	descripcion=models.TextField('Contenido',max_length=250)
	created_update=models.DateTimeField(auto_now_add=True)
	created_init=models.DateTimeField(auto_now=True)
	imagen=CloudinaryField('Imagen',blank=False)


	class Meta:
		verbose_name = 'Noticia'
		verbose_name_plural = 'Modulo de noticias'

	def __unicode__(self):
		try:
			public_id = self.imagen.titulo
		except AttributeError:
			public_id = ''
			return "Proyecto-%s:%s" % (self.titulo, public_id)


class Galeria(models.Model):

	descripcion=models.AutoField(primary_key=True)
	galeria_img=CloudinaryField('Imagen de Galeria',blank=False)
	created_update = models.DateTimeField(auto_now_add=True)
	created_init = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'imagenes'
		verbose_name_plural = 'Modulo de Galeria'

	def __unicode__(self):
		try:
			public_id = self.galeria_img.titulo
		except AttributeError:
			public_id = ''
			return "Nosotros-%s:%s" % (self.descripcion, public_id)

class NoticiasAdmin(models.Model):

	descripcion=models.TextField('Descripcion de la noticia',max_length=100)
	titulo=models.CharField(max_length=200)
	image=CloudinaryField()
	created_update = models.DateTimeField(auto_now_add=True)
	created_init = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Noticia inicial'
		verbose_name_plural = 'Modulo de Noticias'

	def __unicode__(self):
		try:
			public_id = self.imagen_perfil.titulo
		except AttributeError:
			public_id = ''
			return "Team-%s:%s" % (self.descripcion, public_id)


