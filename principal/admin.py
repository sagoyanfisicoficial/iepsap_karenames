from principal.models import Noticia,Galeria,NoticiasAdmin
from django.contrib import admin


@admin.register(Noticia)
class NoticiaAdmin(admin.ModelAdmin):
    model = Noticia
    list_display =('titulo',
                   'descripcion',
                   'created_update',
                   'created_init',
                   )

@admin.register(Galeria)
class GaleriaAdmin(admin.ModelAdmin):
    model = Galeria
    list_display =('descripcion',
                   'created_update',
                   'created_init',
                   )
    class User:
        list_display_links = ('',)

@admin.register(NoticiasAdmin)
class NoticiasAdmin2(admin.ModelAdmin):
    model = NoticiasAdmin
    list_display =('descripcion',
                   'titulo',
                   'created_update',
                   'created_init',
                   )
    class User:
        list_display_links = ('',)