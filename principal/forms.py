#encoding:utf-8 
from django.forms import ModelForm
from django import forms

class ContactoForm(forms.Form):
	correo = forms.EmailField(label='Email',required=False)

	mensaje = forms.CharField(widget=forms.Textarea(attrs = {"id": "email", "type": "email", "class": "email", "required": "required"}))

