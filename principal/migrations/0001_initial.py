# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cloudinary.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Galeria',
            fields=[
                ('descripcion', models.AutoField(serialize=False, primary_key=True)),
                ('galeria_img', cloudinary.models.CloudinaryField(max_length=255, verbose_name=b'Imagen de Galeria')),
                ('created_update', models.DateTimeField(auto_now_add=True)),
                ('created_init', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'My image',
                'verbose_name_plural': 'My images',
            },
        ),
        migrations.CreateModel(
            name='Noticia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('imagen', cloudinary.models.CloudinaryField(max_length=255, verbose_name=b'Imagen')),
                ('titulo', models.CharField(max_length=200, verbose_name=b'Titulo')),
                ('descripcion', models.TextField(max_length=250, verbose_name=b'Contenido')),
                ('created_update', models.DateTimeField(auto_now_add=True)),
                ('created_init', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'My image',
                'verbose_name_plural': 'My images',
            },
        ),
        migrations.CreateModel(
            name='NoticiasAdmin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descripcion', models.TextField(max_length=100, verbose_name=b'Descripcion de la noticia')),
                ('titulo', models.CharField(max_length=200)),
                ('image', cloudinary.models.CloudinaryField(max_length=255)),
                ('created_update', models.DateTimeField(auto_now_add=True)),
                ('created_init', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'My image',
                'verbose_name_plural': 'My images',
            },
        ),
    ]
